﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    UIManager uiManager;
    Rigidbody2D rb2d;

    float posToMoveX;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        uiManager = GameObject.FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (uiManager.IsTapToStartHidden())
                        uiManager.HideTapToStartInfo();
                    rb2d.MovePosition(new Vector2(transform.position.x + touchPos.x * Time.deltaTime, transform.position.y));
                    break;
                case TouchPhase.Stationary:
                    rb2d.MovePosition(new Vector2(transform.position.x + touchPos.x * Time.deltaTime, transform.position.y));
                    break;
                case TouchPhase.Moved:
                    rb2d.MovePosition(new Vector2(transform.position.x + touchPos.x * Time.deltaTime, transform.position.y));
                    break;
                case TouchPhase.Ended:
                    rb2d.MovePosition(new Vector2(transform.position.x, transform.position.y));
                    break;
            }
        }
    }
}
