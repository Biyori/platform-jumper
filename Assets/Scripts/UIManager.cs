﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject tapToStartInfo;

    bool isTapToStartHidden;

    // Start is called before the first frame update
    void Start()
    {
        isTapToStartHidden = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HideTapToStartInfo()
    {
        isTapToStartHidden = true;
        tapToStartInfo.SetActive(false);
    }

    public bool IsTapToStartHidden()
    {
        return isTapToStartHidden;
    }
}
